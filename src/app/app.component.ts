import { Component } from '@angular/core';
import { Product } from './model/product';
import { CustomerService } from './services/customer.service';
import { ProductService } from './services/product.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title: string = 'angular101 🔥';
  //total = 0;

  constructor(public productService: ProductService, public customerService: CustomerService) {
    this.productService.getProducts();
  }

  get filteredProducts(): Product[] {
   return this.productService.products.filter((product) => product.price > 10); // Tous les produits sont sup à 10, il faut augmenter la valeur pour filtrer les produits.
  }

  addAndDecrease(product: Product) {
    this.customerService.addProduct(product);
    this.productService.decreaseStock(product);
  }
}
