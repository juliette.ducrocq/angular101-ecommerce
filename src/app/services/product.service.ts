import { Injectable } from '@angular/core';
import { Product } from '../model/product';


@Injectable({
  providedIn: 'root'
})
export class ProductService {
  products: Product[] = [];
  constructor() {
  }

  getProducts() {
    setTimeout(() => {
      this.products = [
        new Product('Men Sweatshirt', 'C0D1NG_TH3_W0RLD BIO HOODIE - MEN.', 'https://s3.eu-central-1.amazonaws.com/balibart-s3/Products/5acf344514006a7fe670e2eb/Mockups/front.png', 39, 13),
        new Product('Men T-Shirt', 'BIO T-SHIRT WITH CREWNECK - MEN.', 'https://s3.eu-central-1.amazonaws.com/balibart-s3/Products/5b2911e4ab33424aec592bd6/Mockups/front.png', 19, 2),
        new Product('T-Shirt women', 'BIO T-SHIRT WITH CREWNECK - WOMEN.', 'https://s3.eu-central-1.amazonaws.com/balibart-s3/Products/5b290d26ab33424aec592bd4/Mockups/front.png', 30, 8),
        new Product('Tote bag', 'C0D1NG_TH3_W0RLD, BIO TOTE BAG.', 'https://s3.eu-central-1.amazonaws.com/balibart-s3/Products/5acf160814006a7fe670e2dd/Mockups/front.png', 12.50, 5)
      ];
    }, 2000);
  }

  isLast(product: Product) {
    return product.stock === 1;
  }

  isAvailable(product: Product) {
    return product.stock > 0;
  }

  decreaseStock(product: Product): void {
    product.stock--;
  }
}