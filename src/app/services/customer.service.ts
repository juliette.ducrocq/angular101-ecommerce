import { Injectable } from '@angular/core';
import { Product } from '../model/product';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  basket: Product[] = [];
  total = 0;

  constructor() {}

  addProduct(product: Product) {
    this.basket.push(product);
  }

  get Total() {
    return this.basket.reduce((accumulator, productInBasket) => (accumulator += productInBasket.price), 0);
  }
}
